<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Product extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'stock', 'price'
    ];

    public static function findOne($where = array())
    {
        $find = DB::table('products')
            ->select('products.*')
            ->where($where)
            ->first();
        return $find;
    }

    public static function getAll($where = array())
    {
        $find = DB::table('products')
            ->select('products.*')
            ->where($where)
            ->get()->toArray();
        return $find;
    }

    public static function getByIndexId($where = array())
    {
        $return = [];
        $find = DB::table('products')
            ->select('products.*')
            ->where($where)
            ->get()->toArray();
        if($find){
            foreach ($find as $key => $value) {
                $return[$value->id] = $value;
            }
        }
        return $return;
    }

    public static function getByIndexIdWhereIn($where = array())
    {
        $return = [];
        $find = DB::table('products')
            ->select('products.*')
            ->whereIn('id', $where)
            ->get()->toArray();
        if($find){
            foreach ($find as $key => $value) {
                $return[$value->id] = $value;
            }
        }
        return $return;
    }
}
