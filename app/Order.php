<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Order extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code_order', 'id_customer', 'total_price', 'status'
    ];

    public static function findOne($where = array())
    {
        $find = DB::table('orders')
            ->select('orders.*', 'customers.name as name_customer')
            ->join('customers','orders.id_customer','=','customers.id', 'left')
            ->where($where)
            ->first();
        return $find;
    }

    public static function getAll($where = array())
    {
        $find = DB::table('orders')
            ->select('orders.*', 'customers.name as name_customer')
            ->join('customers','orders.id_customer','=','customers.id', 'left')
            ->where($where)
            ->get()->toArray();
        return $find;
    }
}
