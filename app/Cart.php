<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class Cart extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_customer', 'total_price'
    ];

    public static function findOne($where = array())
    {
        $find = DB::table('carts')
            ->select('carts.*', 'customers.name as name_customer')
            ->join('customers','carts.id_customer','=','customers.id', 'left')
            ->where($where)
            ->first();
        return $find;
    }

    public static function getAll($where = array())
    {
        $find = DB::table('carts')
            ->select('carts.*', 'customers.name as name_customer')
            ->join('customers','carts.id_customer','=','customers.id', 'left')
            ->where($where)
            ->get()->toArray();
        return $find;
    }
}
