<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class CartDetail extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_cart', 'id_product', 'stock', 'total_price'
    ];

    public static function findOne($where = array())
    {
        $find = DB::table('cart_details')
            ->select('cart_details.*', 'carts.id_customer as id_customer', 'products.name as name_product')
            ->join('products','cart_details.id_product','=','products.id', 'left')
            ->join('carts','carts.id','=','cart_details.id_cart', 'left')
            ->where($where)
            ->first();
        return $find;
    }

    public static function getAll($where = array())
    {
        $find = DB::table('cart_details')
            ->select('cart_details.*', 'carts.id_customer as id_customer', 'products.name as name_product')
            ->join('products','cart_details.id_product','=','products.id', 'left')
            ->join('carts','carts.id','=','cart_details.id_cart', 'left')
            ->where($where)
            ->get()->toArray();
        return $find;
    }

    public static function getTotal($where = array())
    {
        $find = DB::table('cart_details')
            ->select(DB::raw('SUM(total_price) as total_price'))
            ->where($where)
            ->groupBy('id_cart')
            ->first();
        return $find;
    }  

    public static function getByIndexId($where = array())
    {
        $return = [];
        $find = DB::table('cart_details')
            ->select('cart_details.*', 'carts.id_customer as id_customer', 'products.name as name_product')
            ->join('products','cart_details.id_product','=','products.id', 'left')
            ->join('carts','carts.id','=','cart_details.id_cart', 'left')
            ->where($where)
            ->get()->toArray();
        if($find){
            foreach ($find as $key => $value) {
                $return[$value->id_product] = $value;
            }
        }
        return $return;
    }

    public static function deleteProduct($where = array()){
        $delete = DB::table('cart_details')
            ->select('cart_details.*', 'carts.id_customer as id_customer')
            ->join('carts','carts.id','=','cart_details.id_cart', 'left')
            ->where($where)
            ->delete();
       return $delete;
    }
}
