<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Customer;
use App\Order;
use App\OrderDetail;
use App\Cart;
use App\CartDetail;
use App\Product;

class OrderController extends Controller
{
    public function getOrder(Request $request){
        $id = $request->id;
        if($id != null){ //cek apakah id tidak null
                
                $order = self::getOrderWithDetail($id);
                if($order){
                    if($order->status == 1){
                        $status_code = 200;
                        $message = 'Order sudah dibayar.';
                        $data = $order;
                    }elseif($order->status == 2){
                        $status_code = 204;
                        $message = 'Order sudah kadaluarsa.';
                        $data = $order;
                    }else{
                        /* cek apakah masih tersedia order ini */
                        $is_available = self::checkIsAvailable($order->created_at);
                        if(!$is_available){

                            $update_order = self::updateStatusExpired($order->id);
                            if($update_order){
                                $status_code = 204;
                                $message = 'Order sudah kadaluarsa.';
                                $data = $order;
                            }else{
                                $status_code = 500;
                                $message = 'terdapat kesalahan pada server';
                                $data = [];
                            }
                        }else{
                            $status_code = 202;
                            $message = 'Berhasil Mengambil Data.';
                            $data = $order;
                        }
                    }
                }else{
                    $status_code = 400;
                    $message = 'Order tidak ditemukan';
                    $data = [];
                }
        }else{
            $status_code = 401;
            $message = 'ID tidak boleh kosong';
            $data = [];
        }

        return response()->json([
            'status_code' => $status_code,
            'message' => $message,
            'data' => $data
        ]);
    }

    public function addToOrder(Request $request){
        $id_customer = $request->id_customer;
        if($id_customer != null){ //cek apakah terdapat id_customer 
            
            $customer = Customer::findOne(['customers.id' => $id_customer]);
            if($customer){ // cek customer terdaftar
                
                // validasi input
                $validator = Validator::make($request->all(), [
                    'id_product' => ['required'],
                    'stock' => ['required'],
                ]);
                if ($validator->fails()) {
                    return response()
                        ->json([
                            'status_code' => 400,
                            'message' => 'Error Validation',
                            'data' => [],
                            'validations' => $validator->errors()
                        ], 422);
                }

                $get_order = Order::getAll();
                $count_order = count($get_order); // hitung berapa order terdaftar untuk mendapatkan last code order
                $last_no = $count_order + 1;
                $code_order = 'ORDER-'.date('Y-m-d')."/".$last_no;
                $save_order = [
                    'code_order' => $code_order,
                    'id_customer' => $id_customer,
                    'total_price' => 0,
                    'status' => 0,
                ];
                $order = Order::create($save_order);
                $order->save();

                /* init */
                $is_in_cart = 1; //state is in cart
                $is_in_list = 1; //state is in list
                $is_in_stock = 1; //state is in stock
                $id_product = $request->id_product;
                $stock = $request->stock;
                $error_message_cart = ''; //error message untuk product tidak ada dalam cart
                $error_message_product = ''; //error message untuk product tidak ada dalam list product
                $error_message_stock = ''; //error message untuk product stoknya kosong
                $array_save = [];

                /* get cart detail */
                $get_cart = CartDetail::getByIndexId(['carts.id_customer' => $id_customer]);
                
                foreach ($id_product as $key => $value) { //looping post id product
                    $input_stock = 0;
                    if(isset($stock[$key])){
                        $input_stock = $stock[$key];
                    }
                    /* get product */
                    $product = Product::findOne(['products.id' => $value]);
                    if($product){
                        if(!isset($get_cart[$value])){
                            $is_in_cart = 0; //item tidak ada dalam cart
                            $error_message_cart .= 'Produk '.$product->name.' tidak ada dalam keranjang anda. ';
                        }else{
                            if($input_stock > 0 && $input_stock <= $product->stock){ //cek apakah stok tersedia
                                $data_save = [
                                    "id_order" => $order->id,
                                    "id_product" => $value,
                                    "stock" => $input_stock,
                                    "total_price" => $input_stock * $product->price
                                ];
                                array_push($array_save, $data_save);

                                /* kurangi ke product */
                                $update_data = [
                                    'stock' => $product->stock - $input_stock ,
                                ];
                                $update = Product::where('id', $value);
                                $update->update($update_data);

                            }else{
                                $is_in_stock = 0;
                                $error_message_stock .= 'Produk '.$product->name.' stok tidak tersedia. ';
                            }
                        }
                    }else{
                        $is_in_list = 0; //item tidak ada dalam product
                        $error_message_product .= 'Produk id '.$value.' tidak terdaftar.';
                    }
                }

                if($is_in_cart && $is_in_list && $is_in_stock){
                    
                    foreach ($array_save as $key => $value) {
                        /* save ke order detail */
                        OrderDetail::create(
                            $value
                        );

                        $id_cart = null; //init id cart
                        $get_cart_detail = CartDetail::findOne(['carts.id_customer' => $id_customer, 'cart_details.id_product' => $value['id_product']]); //get data cart detail untuk mendapatkan id_cart
                        if($get_cart_detail){
                            $id_cart = $get_cart_detail->id_cart;
                        }
                        $delete_cart_detail = CartDetail::deleteProduct(['carts.id_customer' => $id_customer, 'cart_details.id_product' => $value['id_product']]); //delete Product dari cart
                        if($id_cart){
                        $get_cart_detail = CartDetail::getAll(['cart_details.id_cart' => $id_cart]); //get data cart detail jika kosong maka cart akan di hapus
                            if(!$get_cart_detail){
                                $delete_cart = Cart::where('id', $id_cart)->delete(); //jika kosong hapus
                            }else{
                                self::updateTotalPriceCart($id_cart); //jika ada update total price
                            }
                        }
                    }

                    self::updateTotalPrice($order->id); //update total price

                    $status_code = 201;
                    $message = 'Berhasil melakukan Checkout';
                    $data = [];

                }else{
                    
                    /* order tidak akan di proses jika salah satu produk ada yang stoknya kurang, atau tidak terdaftar di keranjangnya sebelumnya, atau produk tidak ada dalam list */
                    if(!$is_in_stock){
                        $status_code = 400;
                        $message = $error_message_stock;
                        $data = [];
                    }
                    
                    if(!$is_in_cart){
                        $status_code = 400;
                        $message = $error_message_cart;
                        $data = [];
                    }

                    if(!$is_in_list){
                        $status_code = 400;
                        $message = $error_message_product;
                        $data = [];
                    }

                    /* pengembalian stok yang sudah di kurangi sebelumnya*/
                    if($array_save){
                        foreach ($array_save as $key => $value) {
                            $product = Product::findOne(['products.id' => $value['id_product']]);

                            /* tambah stok ke product */
                            $update_data = [
                                'stock' => $product->stock + $value['stock'],
                            ];
                            $update = Product::where('id', $value['id_product']);
                            $update->update($update_data);
                        }
                    }

                    /* order delete */
                    $delete = Order::where('id', $order->id)->delete();

                }

            }else{
                $status_code = 401;
                $message = 'Customer tidak terdaftar';
                $data = [];
            }

        }else{
            $status_code = 401;
            $message = 'ID Customer tidak boleh kosong';
            $data = [];
        }

        return response()->json([
            'status_code' => $status_code,
            'message' => $message,
            'data' => $data
        ]);
    }

    public function payment(Request $request){
        $id = $request->id;
        if($id != null){ //cek apakah id tidak null
                $get_order = Order::findOne(['orders.id' => $id]);    
                if($get_order){
                    if($get_order->status == 0){
                        /* cek apakah masih tersedia order ini */
                        $is_available = self::checkIsAvailable($get_order->created_at);
                        if(!$is_available){
                            $update_order = self::updateStatusExpired($get_order->id);
                            if($update_order){
                                $status_code = 204;
                                $message = 'Order sudah kadaluarsa.';
                                $data = $get_order;
                            }else{
                                $status_code = 500;
                                $message = 'terdapat kesalahan pada server';
                                $data = [];
                            }
                        }else{
                            $data_update = [
                                'status' => 1
                            ];
                            Order::where('id', $id)->update($data_update);
    
                            $status_code = 201;
                            $message = 'Berhasil Melakukan Pembayarans.';
                            $data = $get_order;
                        }
                    }elseif($get_order->status == 1){
                        $status_code = 200;
                        $message = 'Order sudah dibayar.';
                        $data = $get_order;
                    }elseif($get_order->status == 2){
                        $status_code = 204;
                        $message = 'Order sudah kadaluarsa.';
                        $data = $get_order;
                    }
                }else{
                    $status_code = 400;
                    $message = 'Data Order tidak di temukan';
                    $data = [];
                }
        }else{
            $status_code = 401;
            $message = 'ID tidak boleh kosong';
            $data = [];
        }

        return response()->json([
            'status_code' => $status_code,
            'message' => $message,
            'data' => $data
        ]);
    }

    /* untuk cron job pengecekkan apakah ada product yang sudah melampaui batas waktu pembayaran */
    public function checkExpire(){
        $get_order = Order::getAll(['orders.status' => 0]);
        if($get_order){
            foreach ($get_order as $key => $value) {
                $is_available = self::checkIsAvailable($value->created_at);
                if(!$is_available){
                    $update_order = self::updateStatusExpired($value->id);
                }
            }
        }

        return response()->json([
            'status_code' => 200,
            'message' => 'Berhasil menjalankan function checkExpire',
            'data' => []
        ]);
    }

    private static function getOrderWithDetail($id){
        $order = Order::findOne(['orders.id' => $id]);
        if($order){
            // init object detail order
            $order->order_detail = [];
                
            // get detail cart
            $order_detail = OrderDetail::getAll(['order_details.id_order' => $id]);
            if($order_detail){
                $order->order_detail = $order_detail;    
            }
        }
        return $order;
    }

    private static function updateTotalPrice($id_order){
        $get_total = OrderDetail::getTotal(['id_order' => $id_order]);
        if($get_total){
            /* update cart total price */
            $data_update_order = [
                'total_price' => $get_total->total_price,
            ];
            $update_order = Order::where('id', $id_order)->update($data_update_order);
        }
    }

    private static function updateTotalPriceCart($id_cart){
        $get_total = CartDetail::getTotal(['id_cart' => $id_cart]);
        if($get_total){
            /* update cart total price */
            $data_update_cart = [
                'total_price' => $get_total->total_price,
            ];
            $update_cart = Cart::where('id', $id_cart)->update($data_update_cart);
        }
    }

    private static function checkIsAvailable($created_at){
        $is_available = 1; //init
        $now = date('Y-m-d H:i:s'); //init waktu saat ini

        $datetime1 = strtotime($created_at);
        $datetime2 = strtotime($now);
        $interval  = abs($datetime2 - $datetime1);
        $minutes   = round($interval / 60);
        if($minutes > 10){ // jika lebih dari 10 menit maka hapus data
            $is_available = 0;
        }
        
        return $is_available;
    }

    private static function updateStatusExpired($id_order){
        /* update status ke 2 */
        $data_update = [
            'status' => 2
        ];
        $update_order = Order::where('id', $id_order)->update($data_update);
        if($update_order){
            /* fungsi untuk mengembalikan stock */
            $get_order_detail = OrderDetail::getAll(['id_order' => $id_order]);
            if($get_order_detail){
                foreach ($get_order_detail as $key => $value) {
                    $get_product = Product::findOne(['id' => $value->id_product]);
                    if($get_product){
                        $data_update_product = [
                            'stock' => $get_product->stock + $value->stock
                        ];
                        Product::where('id', $value->id_product)->update($data_update_product);
                    }
                }
            }
        }

        return $update_order;
    }
}
