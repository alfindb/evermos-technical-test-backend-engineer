<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Product;

class ProductController extends Controller
{
    public function getProduct(Request $request){
        /* init */
        $where = [];
        $id = $request->id;
        if($id != null){ //cek apakah get all atau get by id
            $where['products.id'] = $id;
        }
        
        /* get product */
        $product = Product::getAll($where);
        if($product){
            $status_code = 200;
            $message = 'Berhasil Mengambil Data.';
            $data = $product;
        }else{
            $status_code = 204;
            $message = 'Data Tidak di temukan.';
            $data = [];
        }

        return response()->json([
            'status_code' => $status_code,
            'message' => $message,
            'data' => $data
        ]);
    }

    public function create(Request $request){
        /* validasi input */
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'stock' => ['required', 'numeric'],
            'price' => ['required', 'numeric'],
        ]);
        if ($validator->fails()) {
            return response()
                ->json([
                    'status_code' => 400,
                    'message' => 'Error Validation',
                    'data' => [],
                    'validations' => $validator->errors()
                ], 422);
        }

        $save_data = [
            'name' => $request->name,
            'stock' => $request->stock,
            'price' => $request->price,
        ];
        $insert = Product::create($save_data);
        $insert->save();
        if($insert){
            $status_code = 201;
            $message = 'Berhasil Menyimpan data';
            $data = $insert;
        }else{
            $status_code = 400;
            $message = 'Gagal Menyimpan data';
            $data = [];
        }

        return response()->json([
            'status_code' => $status_code,
            'message' => $message,
            'data' => $data
        ]);
    }

    public function update(Request $request){
        $id = $request->id;
        if($id != null){ //cek apakah id product ada atau tidak
            /* validasi input */
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string'],
                'stock' => ['required', 'numeric'],
                'price' => ['required', 'numeric'],
            ]);
    
            if ($validator->fails()) {
                return response()
                    ->json([
                        'status_code' => 400,
                        'message' => 'Error Validation',
                        'data' => [],
                        'validations' => $validator->errors()
                    ], 422);
            }

            $update_data = [
                'name' => $request->name,
                'stock' => $request->stock,
                'price' => $request->price,
            ];
            $update = Product::where('id', $id);
            $update->update($update_data);
            if($update){
                $data_product = Product::findOne(['id' => $id]);

                $status_code = 201;
                $message = 'Berhasil Mengubah data';
                $data = $data_product;
            }else{
                $status_code = 400;
                $message = 'Gagal Mengubah data';
                $data = [];
            }

        }else{
            $status_code = 400;
            $message = 'ID tidak boleh kosong';
            $data = [];
        }
        
        return response()->json([
            'status_code' => $status_code,
            'message' => $message,
            'data' => $data
        ]);
    }
}
