<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Customer;
use App\Cart;
use App\CartDetail;
use App\Product;

class CartController extends Controller
{
    public function getCart(Request $request){
        $id_customer = $request->id_customer;
        if($id_customer != null){ //cek apakah id_customer tidak null
            $customer = Customer::findOne(['customers.id' => $id_customer]);
            if($customer){ // cek customer terdaftar
                $cart = self::getCartWithDetail($id_customer);
                if($cart){
                    $status_code = 200;
                    $message = 'Berhasil Mengambil Data.';
                    $data = $cart;
                }else{
                    $status_code = 400;
                    $message = 'Keranjang Kosong';
                    $data = [];
                }
            }else{
                $status_code = 400;
                $message = 'Customer tidak terdaftar';
                $data = [];
            }
        }else{
            $status_code = 400;
            $message = 'ID Customer tidak boleh kosong';
            $data = [];
        }

        return response()->json([
            'status_code' => $status_code,
            'message' => $message,
            'data' => $data
        ]);
    }

    public function addToCart(Request $request){
        $id_customer = $request->id_customer;
        if($id_customer != null){ //cek apakah terdapat id_customer 
            
            $customer = Customer::findOne(['customers.id' => $id_customer]);
            if($customer){ // cek customer terdaftar
                
                // validasi input
                $validator = Validator::make($request->all(), [
                    'id_product' => ['required', 'numeric'],
                    'stock' => ['required', 'numeric'],
                ]);
                if ($validator->fails()) {
                    return response()
                        ->json([
                            'status_code' => 400,
                            'message' => 'Error Validation',
                            'data' => [],
                            'validations' => $validator->errors()
                        ], 422);
                }

                $cart = Cart::findOne(['carts.id_customer' => $id_customer]); // cek apakah cart milik customer sudah ada
                if(!$cart){
                    $save_cart = [
                        'id_customer' => $id_customer,
                        'total_price' => 0,
                    ];
                    $insert_cart = Cart::create($save_cart);
                    $insert_cart->save();

                    $cart = $insert_cart;
                }
                
                /* init */
                $status_save = 0; //state
                $id_product = $request->id_product;
                $stock = 1;
                if($stock != null){
                    $stock = $request->stock;
                }
                
                $find_product = Product::findOne(['products.id' => $id_product]);
                if($find_product){
                    if($stock <= $find_product->stock){ //cek ketersediaan stok
                        $where_detail['cart_details.id_cart'] = $cart->id;
                        $where_detail['cart_details.id_product'] = $id_product;
                        $get_cart_detail = CartDetail::findOne($where_detail);
                        if($get_cart_detail){
                            
                            $stock += $get_cart_detail->stock; //jumlahkan dengan stok yang dipesan lakukan pengecekkan kembali
                            if($stock <= $find_product->stock){ //cek ketersediaan stok

                                $update_detail = [
                                    'stock' => $stock,
                                    'total_price' => $stock * $find_product->price
                                ];
                                $update_cart_detail = CartDetail::where([
                                                                    ['id_product', '=', $id_product],
                                                                    ['id_cart', '=', $cart->id],
                                                                ]);
                                $update_cart_detail->update($update_detail);
                                if($update_cart_detail){
                                    $status_save = 1; //berhasil
                                }else{
                                    $status_save = 2; //gagal
                                }

                            }else{
                                $status_save = 3; //melebihi stock
                            }

                        }else{
                            $save_detail = [
                                'id_cart' => $cart->id,
                                'id_product' => $id_product,
                                'stock' => $stock,
                                'total_price' => $stock * $find_product->price
                            ];
                            $insert_cart = CartDetail::create($save_detail);
                            $insert_cart->save();
                            if($insert_cart){
                                $status_save = 1; //berhasil
                            }else{
                                $status_save = 2; //gagal
                            }
                        }

                    }else{
                        $status_save = 3; //melebihi stock
                    }

                    /* return based on status save */
                    if($status_save == 1){
                        self::updateTotalPrice($cart->id); //update total price keranjang

                        $status_code = 201;
                        $message = 'Berhasil menambahkan ke dalam keranjang';
                        $data = [];
                    }elseif($status_save == 2){
                        $status_code = 400;
                        $message = 'Gagal menambahkan ke dalam keranjang';
                        $data = [];
                    }elseif($status_save == 3){
                        $status_code = 400;
                        $message = 'Melebihi stok yang tersedia';
                        $data = [];
                    }else{
                        $status_code = 505;
                        $message = 'Terjadi kesalahan pada server';
                        $data = [];
                    }

                }else{
                    $status_code = 400;
                    $message = 'Produk tidak ditemukan';
                    $data = [];
                }

            }else{
                $status_code = 401;
                $message = 'Customer tidak terdaftar';
                $data = [];
            }

        }else{
            $status_code = 401;
            $message = 'ID Customer tidak boleh kosong';
            $data = [];
        }

        return response()->json([
            'status_code' => $status_code,
            'message' => $message,
            'data' => $data
        ]);
    }

    private static function getCartWithDetail($id_customer){
        $cart = Cart::findOne(['carts.id_customer' => $id_customer]);
        if($cart){
            // init object detail cart
            $cart->cart_detail = [];
                
            // get detail cart
            $cart_detail = CartDetail::getAll(['cart_details.id_cart' => $cart->id]);
            if($cart_detail){
                $cart->cart_detail = $cart_detail;    
            }
        }
        return $cart;
    }

    private static function updateTotalPrice($id_cart){
        $get_total = CartDetail::getTotal(['id_cart' => $id_cart]);
        if($get_total){
            /* update cart total price */
            $data_update_cart = [
                'total_price' => $get_total->total_price,
            ];
            $update_cart = Cart::where('id', $id_cart)->update($data_update_cart);
        }
    }
}
