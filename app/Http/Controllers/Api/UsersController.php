<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\User;

class UsersController extends Controller
{
    public function getAllUsers(Request $request){
        $users = User::getAll();
        if($users){
            $status = true;
            $message = 'Berhasil Mengambil Data.';
            $data = $users;
        }else{
            $status = false;
            $message = 'Gagal Mengambil Data.';
            $data = [];
        }

        return response()->json([
            'success' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }

    public function getUser(Request $request){
        $user_id = $request->id;
        if($user_id == null || $user_id == ''){
            return response()->json([
                'success' => false,
                'message' => 'Data tidak ditemukan.',
                'data' => []
            ]);
        }

        $user = User::findOne(['users.id' => $user_id]);
        if($user){
            $status = true;
            $message = 'Berhasil mengambil data.';
            $data = $user;
        }else{
            $status = false;
            $message = 'Data tidak ditemukan.';
            $data = [];
        }

        return response()->json([
            'success' => $status,
            'message' => $message,
            'data' => $data
        ]);
    }

    public function create(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string'],
            'email' => ['required', 'email', 'string'],
            'password' => ['required', 'string']
        ]);

        if ($validator->fails()) {
            return response()
                ->json([
                    'status' => false,
                    'message' => 'Error Validasi',
                    'data' => [],
                    'validations' => $validator->errors()
                ], 422);
        }

        //cek email sudah ada atau belum
        $cek_email = User::findOne(['users.email' => $request->email]);
        if($cek_email){
            return response()->json([
                'success' => false,
                'message' => 'Email sudah terdaftar',
                'data' => []
            ]);
        }

        $save_data = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ];
        $insert_data = User::create($save_data);
        $insert_data->save();

        if($insert_data){
            return response()->json([
                'success' => true,
                'message' => 'Berhasil menyimpan data',
                'data' => ['id' => $insert_data->id]
            ]);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Gagal menyimpan data',
                'data' => []
            ]);
        }
    }
}
