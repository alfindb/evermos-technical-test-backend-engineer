<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class OrderDetail extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_order', 'id_product', 'stock', 'total_price'
    ];

    public static function findOne($where = array())
    {
        $find = DB::table('order_details')
            ->select('order_details.*', 'products.name as name_product')
            ->join('products','order_details.id_product','=','products.id', 'left')
            ->where($where)
            ->first();
        return $find;
    }

    public static function getAll($where = array())
    {
        $find = DB::table('order_details')
            ->select('order_details.*', 'products.name as name_product')
            ->join('products','order_details.id_product','=','products.id', 'left')
            ->where($where)
            ->get()->toArray();
        return $find;
    }

    public static function getTotal($where = array())
    {
        $find = DB::table('order_details')
            ->select(DB::raw('SUM(total_price) as total_price'))
            ->where($where)
            ->groupBy('id_order')
            ->first();
        return $find;
    }  
}
