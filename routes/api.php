<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    /* product */
    Route::get('/product', 'Api\ProductController@getProduct')->name('product');
    Route::get('/product/{id}', 'Api\ProductController@getProduct')->name('product');
    Route::post('/product/save', 'Api\ProductController@create')->name('product.save');
    Route::put('/product/{id}/update', 'Api\ProductController@update')->name('product.update');
    
    /* cart */
    Route::get('/cart', 'Api\CartController@getCart')->name('cart');
    Route::get('/cart/{id_customer}', 'Api\CartController@getCart')->name('cart');
    Route::post('/cart/{id_customer}/save', 'Api\CartController@addToCart')->name('cart.save');

    /* order */
    Route::get('/order', 'Api\OrderController@getOrder')->name('order');
    Route::get('/order/{id}', 'Api\OrderController@getOrder')->name('order');
    Route::post('/order/{id_customer}/save', 'Api\OrderController@addToOrder')->name('order.save');
    Route::get('/order_check_expire', 'Api\OrderController@checkExpire')->name('order.check_expire');
    Route::post('/order/payment/{id}', 'Api\OrderController@payment')->name('order.payment');
