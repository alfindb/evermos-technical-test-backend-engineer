<?php

use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array_insert = array(
    		array(
	            'name' => 'T-Shirt',
	            'stock' => 10
        	),
            array(
	            'name' => 'Hoodie',
	            'stock' => 5
        	),
            array(
	            'name' => 'Nike Shoes',
	            'stock' => 15
        	),
            array(
	            'name' => 'Keyboard',
	            'stock' => 7
        	),
            array(
	            'name' => 'Speaker',
	            'stock' => 12
        	),
    	);

    	for ($i=0; $i < 5; $i++) { 
	        \App\Product::create(
	        	$array_insert[$i]
	        );
    	}
    }
}
