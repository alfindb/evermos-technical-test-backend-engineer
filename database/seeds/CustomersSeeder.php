<?php

use Illuminate\Database\Seeder;

class CustomersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array_insert = array(
    		array(
	            'name' => 'Alfin',
	            'address' => 'Bandung',
                'phone' => '0987654321'
        	),
            array(
	            'name' => 'Joni',
	            'address' => 'Cimahi',
                'phone' => '1234567890'
        	),
            array(
	            'name' => 'Teja',
	            'address' => 'Jakarta',
                'phone' => '9090909090'
        	),
    	);

    	for ($i=0; $i < 3; $i++) { 
	        \App\Customer::create(
	        	$array_insert[$i]
	        );
    	}
    }
}
