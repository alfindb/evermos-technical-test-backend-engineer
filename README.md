## Installation
- Buka command line pada folder (rekomendasi menggunakan gitbash)
- ketikkan composer install, tunggu hingga proses selesai
- copy file .env.example setelah itu paste di folder yang sama lalu rename file dengan nama .env
- pada file .env, atur database name sesuai dengan nama database yang telah anda buat, dan isi username dengan passwordnya berdasarkan setting user mysql anda
- pada command line ketikkan php artisan key:generate
- lalu pada command line ketikkan php artisan serve
- jika akan melakukan migrasi cukup ketikkan php artisan migrate
- jika tidak akan melakukan migrasi, saya sudah menyediakan file.sql untuk di import yang terdapat di /public/support/db_test_evermos.sql
- selanjutnya lakukan import postman yang terdapat di /public/support/evermos-test-backend.postman_collection.json

## Routing dari API
- Product
    - Get All Product ('/product')
    - Get One Product ('/product/{id}') : wajib menyertakan id product
    - Save Product ('/product/save')
    - Update Product ('/product/{id}/update') : wajib menyertakan id product
- Cart
    - Get Cart ('/cart/{id_customer}') : karena api sederhana tidak menggunakan auth, jadi pada route wajib menyertakan id_customer
    - Add To Cart ('/cart/{id_customer}/save') : karena api sederhana tidak menggunakan auth, jadi pada route wajib menyertakan id_customer
- Order
    - Add To Order ('/order/{id_customer}/save') : wajib menyertakan id customer
    - Get Order ('/order/{id}') : wajib menyertakan id order
    - Payment ('/order/payment/{id)') : wajib menyertakan id order
    - Check Expire ('/order_check_expire') 

## Task 1
1. Pada event 12.12 ini semua orang melakukan order suatu produk secara cepat dan bersamaan sehingga yang terjadi adalah loadout dari server meningkat dan membuat proses suatu order mungkin jadi terhambat meski di lakukan secara bersamaan. Permasalahan stok ini terjadi karena sistem tidak melakukan booking stok yang sudah di pesan terlebih dahulu sehingga disaat masuknya order bersamaan kondisi stok terakhir yang diambil belum di kurangi dengan stok dari salah satu order yang baru masuk. Contohnya Stok terakhir Produk A adalah 12. Lalu masuk Order Y dan Order Z  dimana Order Y memesan produk A sebanyak 8 item, dan Order Z memesan produk A sebanyak 7 item. Karena terhambat koneksi atau permasalahan lainnya yang mempengaruhi waktu proses Order Y lebih cepat melakukan proses dibanding Order Z, Disaat order masuk sistem mengambil stok terakhir dari produk A karena tidak di booking dulu stoknya oleh order Y yang lebih cepat melakukan proses, sehingga Order Z mengambil stok terakhir sebanyak 12. Selanjutnya yang terjadi adalah kedua Order tersebut sama sama diproses sehingga hasilnya Produk A stok akhirnya menjadi minus.
2. Solusi yang saya tawarkan adalah perlu adanya booking stok juga pengecekkan stok berlapis pada input ke cart, dan checkout (add to order), pertama pada saat customer akan memasukkan produk yang dipilih kedalam keranjang maka sistem akan melakukan pengecekkan ketersediaan stok, jika stok tidak tersedia tidak dapat menambahkan produk ke dalam keranjang, namun pada proses ini belum mengurangi stok dari produk yang dipilih. Selanjutnya pada saat checkout disinilah booking stok terjadi. sistem mengambil stok terakhir dari produk lalu melakukan pengecekkan apakah stok yang di pesan sesuai dengan stok yang tersedia, jika sesuai maka sistem akan melakukan update data stok terakhir dari produk dan menyimpannya sehingga jika ada order lain yang masuk maka stok produk yang di ambil adalah stok baru yang telah di kurangi dengan stok order sebelumnya. Karena satu order terdapat banyak produk yang dipesan, saya menambahkan skenario jika terjadi kesalahan pada saat order seperti salah satu produk yang dipesan stoknya sudah habis , atau salah satu produk ternyata idnya tidak terdaftar di sistem , maka akan membatalkan order dan stok yang sudah di booking sebelumnya akan di kembalikkan. Setelah melakukan order, customer wajib melakukan pembayaran dalam waktu 10 menit. Saya sudah menyediakan fungsi yang akan melakukan pengecekkan order berstatus 0 yang sudah lewat dari 10 menit maka akan membatalkan order dan mengembalikan stok stok yang sudah di booking sebelumnya. fungsi ini rencananya akan di jalankan melalui cronjob. selain cronjob ini pada saat melakukan get order dilakukan juga pengecekkan apakah order ini sudah melebihi batas waktu pembayaran atau belum.

## Task 2
- Saya menempatkan task_2 ini pada folder /task_2/index.php
- Untuk menjalankannya jalankan command line di dalam folder task_2 lalu ketikkan php index.php
