-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.13-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for db_test_evermos
CREATE DATABASE IF NOT EXISTS `db_test_evermos` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `db_test_evermos`;

-- Dumping structure for table db_test_evermos.carts
CREATE TABLE IF NOT EXISTS `carts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_customer` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total_price` double NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_test_evermos.carts: ~0 rows (approximately)
/*!40000 ALTER TABLE `carts` DISABLE KEYS */;
/*!40000 ALTER TABLE `carts` ENABLE KEYS */;

-- Dumping structure for table db_test_evermos.cart_details
CREATE TABLE IF NOT EXISTS `cart_details` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_cart` bigint(20) NOT NULL,
  `id_product` bigint(20) NOT NULL,
  `stock` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total_price` double NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_test_evermos.cart_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `cart_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart_details` ENABLE KEYS */;

-- Dumping structure for table db_test_evermos.customers
CREATE TABLE IF NOT EXISTS `customers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_test_evermos.customers: ~3 rows (approximately)
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` (`id`, `name`, `address`, `phone`, `created_at`, `updated_at`) VALUES
	(1, 'Alfin', 'Bandung', '0987654321', '2021-11-12 08:02:25', '2021-11-12 08:02:25'),
	(2, 'Joni', 'Cimahi', '1234567890', '2021-11-12 08:02:25', '2021-11-12 08:02:25'),
	(3, 'Teja', 'Jakarta', '9090909090', '2021-11-12 08:02:25', '2021-11-12 08:02:25');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;

-- Dumping structure for table db_test_evermos.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_test_evermos.migrations: ~12 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2021_11_12_062710_create_customers', 1),
	(2, '2021_11_12_062718_create_products', 1),
	(3, '2021_11_12_063334_create_carts', 1),
	(4, '2021_11_12_063341_create_orders', 1),
	(5, '2021_11_12_070825_create_order_details', 1),
	(6, '2021_11_12_072600_create_cart_details', 1),
	(7, '2021_11_12_164335_modify_column_cart', 2),
	(8, '2021_11_12_164344_modify_column_order', 2),
	(9, '2021_11_12_164534_add_column_product', 2),
	(10, '2021_11_12_164613_add_column_cart_details', 2),
	(11, '2021_11_12_164625_add_column_order_details', 2),
	(12, '2021_11_12_172435_add_column_order_details_timestamps', 3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table db_test_evermos.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code_order` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_customer` bigint(20) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total_price` double NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_test_evermos.orders: ~0 rows (approximately)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

-- Dumping structure for table db_test_evermos.order_details
CREATE TABLE IF NOT EXISTS `order_details` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id_order` bigint(20) NOT NULL,
  `id_product` bigint(20) NOT NULL,
  `stock` int(11) NOT NULL DEFAULT 0,
  `total_price` double NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_test_evermos.order_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `order_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_details` ENABLE KEYS */;

-- Dumping structure for table db_test_evermos.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stock` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `price` double NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_test_evermos.products: ~8 rows (approximately)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `name`, `stock`, `created_at`, `updated_at`, `price`) VALUES
	(1, 'T-Shirt', 100, '2021-11-12 08:02:25', '2021-11-13 01:52:12', 5000),
	(2, 'Hoodie', 100, '2021-11-12 08:02:25', '2021-11-13 01:43:58', 10000),
	(3, 'Nike Shoes', 100, '2021-11-12 08:02:25', '2021-11-13 11:18:48', 3000),
	(4, 'Keyboard', 100, '2021-11-12 08:02:26', '2021-11-13 11:18:48', 4000),
	(5, 'Speaker', 100, '2021-11-12 08:02:26', '2021-11-12 08:02:26', 2000),
	(6, 'Beras', 100, '2021-11-12 08:33:35', '2021-11-12 17:55:05', 1000),
	(7, 'Sepatu', 100, '2021-11-12 09:00:19', '2021-11-12 09:15:18', 7000),
	(8, 'Earphone', 100, '2021-11-12 16:53:47', '2021-11-13 01:52:12', 2500);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
