<?php

/* init */
$map = [
    ['#','#','#','#','#','#','#','#'],
    ['#','.','.','.','.','.','.','#'],
    ['#','.','#','#','#','.','.','#'],
    ['#','.','.','.','#','.','#','#'],
    ['#','X','#','.','.','.','.','#'],
    ['#','#','#','#','#','#','#','#'],
];

$treasure = [
    '4' => [
        '3' => 1,
        '5' => 1,
    ],
    '2' => [
        '5' => 1,
        '6' => 1
    ],
    '3' => [
        '5' => 1
    ]
];

foreach ($map as $key => $tr) {
    if($key > 0 ){
        echo "\n";
    }
    foreach ($tr as $k => $td) {
        echo $td;
    }
}

echo "\n-----------------------------";
/* input north */
$counter = 0; 
echo "\nUp/North A step(s): ";
$north = trim(fgets(STDIN));
while ($north == 0 && $counter == 0) {
    echo "Tidak boleh input 0";
    echo "\nUp/North A step(s): ";
    $north = trim(fgets(STDIN));
    if($north == 0){
        $counter = 0;
    }else{
        $counter++;
    }
}

/* input East */
$counter = 0;
echo "\nRight/East B step(s): ";
$east = trim(fgets(STDIN));
while ($east == 0 && $counter == 0) {
    echo "Tidak boleh input 0";
    echo "\nRight/East B step(s): ";
    $east = trim(fgets(STDIN));
    if($east == 0){
        $counter = 0;
    }else{
        $counter++;
    }
}

/* Input South */
$counter = 0;
echo "\nDown/South C step(s): ";
$south = trim(fgets(STDIN));
while ($south == 0 && $counter == 0) {
    echo "Tidak boleh input 0";
    echo "\nDown/South C step(s): ";
    $south = trim(fgets(STDIN));
    if($south == 0){
        $counter = 0;
    }else{
        $counter++;
    }
}
echo "\n-----------------------------";

/* init */
/* $map[y][x] */
$status = 1;
$start_point_x = 1;
$start_point_y = 4;
/* langkah north */
$end_point_y = $start_point_y - $north;
for ($i=$start_point_y; $i <= $end_point_y; $i--) { 
    if( isset($map[$i][$start_point_x]) ){
        if($map[$i][$start_point_x] == '#'){
            $status = 0;
        }
    }else{
        $status = 0;
    }
}
$start_point_y -= $north;

/* langkah east */
if($status == 1){
    $end_point_x = $start_point_x + $east;
    for ($i=$start_point_x; $i <= $end_point_x; $i++) { 
        if(isset($map[$start_point_y][$i])){
            if($map[$start_point_y][$i] == '#'){
                $status = 0;
            }
        }else{
            $status = 0;
        }
    }
    $start_point_x += $east;
}

/* langkah south */
if($status == 1){
    $end_point_y = $start_point_y + $south;
    for ($i=$start_point_y; $i <= $end_point_y; $i++) { 
        if(isset($map[$i][$start_point_x])){
            if($map[$i][$start_point_x] == '#'){
                $status = 0;
            }
        }else{
            $status = 0;
        }
    }
    $start_point_y += $south;
}


// echo "\n";
// foreach ($map as $key => $tr) {
//     if($key > 0 ){
//         echo "\n";
//     }
//     foreach ($tr as $k => $td) {
//         if($key == $start_point_y && $k == $start_point_x){
//             echo "X";
//         }else{
//             echo $td;
//         }
//     }
// }

echo "\nPossible Treasure : \n";
foreach ($map as $key => $tr) {
    if($key > 0 ){
        echo "\n";
    }
    foreach ($tr as $k => $td) {
        if(isset($treasure[$key][$k]) == 1){
            echo "$";
        }else{
            echo $td;
        }
    }
}

echo "\n-----------------------------\n";
if($status == 1){
    echo "Congrats, You Got Treasure !!!";
}else{
    echo "Sorry, You Hit Obstacle";
}

?>